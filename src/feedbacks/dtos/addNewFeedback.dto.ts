import { IsNumber, IsString } from 'class-validator';

export class AddNewFeedbackDto {
  @IsString()
  content!: string;

  @IsNumber()
  rating!: number;

  @IsString()
  senderId!: string;

  @IsString()
  receiverId!: string;

  @IsString()
  topicId!: string;
}
