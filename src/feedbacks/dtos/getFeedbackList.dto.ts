import { IsOptional, IsString } from 'class-validator';
import { PaginationMeta } from '../../shared/dtos';

export class GetFeedbackListDto extends PaginationMeta {
  @IsOptional()
  @IsString()
  topicId?: string;

  @IsOptional()
  @IsString()
  senderId?: string;

  @IsOptional()
  @IsString()
  receiverId?: string;
}
