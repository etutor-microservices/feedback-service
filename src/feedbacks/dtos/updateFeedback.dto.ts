import { PartialType, PickType } from '@nestjs/mapped-types';
import { AddNewFeedbackDto } from '.';

export class UpdateFeedbackDto extends PartialType(
  PickType(AddNewFeedbackDto, ['content', 'rating'] as const),
) {}
