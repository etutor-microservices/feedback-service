import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from } from 'rxjs';
import { AddNewFeedbackDto, UpdateFeedbackDto } from './dtos';
import { Feedback, FeedbackDocument } from './schemas';

@Injectable()
export class FeedbacksService {
  constructor(
    @InjectModel(Feedback.name)
    private readonly _feedbackModel: Model<FeedbackDocument>,
  ) {}

  findListForTarget(topicId?: string, senderId?: string, receiverId?: string) {
    let whereClause = {};

    if (!!topicId) whereClause = { topicId };
    if (!!senderId) whereClause = { senderId };
    if (!!receiverId) whereClause = { receiverId };

    return from(
      this._feedbackModel.where(whereClause).sort({ createdAt: 'desc' }).exec(),
    );
  }

  addNew(dto: AddNewFeedbackDto) {
    const feedback = new this._feedbackModel(dto);
    return from(feedback.save());
  }

  update(id: string, dto: UpdateFeedbackDto) {
    return from(
      this._feedbackModel.findByIdAndUpdate(
        id,
        {
          ...dto,
          updatedAt: new Date(),
        },
        { returnOriginal: false },
      ),
    );
  }

  remove(id: string) {
    return from(this._feedbackModel.findByIdAndRemove(id));
  }
}
