import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type FeedbackDocument = Feedback & Document;

@Schema({ timestamps: { createdAt: true, updatedAt: false } })
export class Feedback {
  @Prop()
  content!: string;

  @Prop()
  rating!: number;

  @Prop()
  senderId!: string;

  @Prop()
  receiverId!: string;

  @Prop()
  topicId!: string;

  @Prop({ default: null })
  updatedAt?: Date;
}

export const FeedbackSchema = SchemaFactory.createForClass(Feedback);
