import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { GetFeedbackListDto } from '../dtos';

@Injectable()
export class GetFeedbackListDtoValidationPipe
  implements PipeTransform<GetFeedbackListDto>
{
  transform(value: GetFeedbackListDto, _: ArgumentMetadata) {
    const { topicId, senderId, receiverId } = value;

    if (!topicId && !senderId && !receiverId)
      throw new BadRequestException(
        'One of these topicId, senderId or receiverId has to be defined!',
      );

    return value;
  }
}
